# url: https://leetcode.com/problems/merge-two-binary-trees/
# writtenby :datla

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        if t1 == None: 
            return t2 
        elif t2 == None:
            return t1 
        t3 = TreeNode(t1.val + t2.val)
        
    
        if t1.left != None and t2.left != None:
            t3.left = self.mergeTrees(t1.left, t2.left)
        elif t1.left == None and t2.left == None:
            pass
        elif t1.left == None:
            t3.left = t2.left
        elif t2.left == None:
            t3.left = t1.left
            
     
        if t1.right != None and t2.right != None:
            t3.right = self.mergeTrees(t1.right, t2.right)
        elif t1.right == None and t2.right == None:
            pass
        elif t1.right == None:
            t3.right = t2.right
        elif t2.right == None:
            t3.right = t1.right
        
        return t3