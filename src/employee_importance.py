# url: https://leetcode.com/problems/employee-importance/
# writtenby: datla

"""
# Definition for Employee.
class Employee:
    def __init__(self, id: int, importance: int, subordinates: List[int]):
        self.id = id
        self.importance = importance
        self.subordinates = subordinates
"""

class Solution:
    def getImportance(self, employees: List['Employee'], id: int) -> int:
        for x,y in enumerate(employees):
            if y.id == id:
                j = x
        employee = employees[j]
        if not employee.subordinates: #if no subordinates
            return employee.importance
        else:
            total_subordinate_importance = 0
            for i in employee.subordinates:
                total_subordinate_importance += self.getImportance(employees, i)
            return employee.importance + total_subordinate_importance