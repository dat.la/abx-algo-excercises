# url: https://leetcode.com/problems/average-of-levels-in-binary-tree/
# writtenby: datla

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:
        def average(t: TreeNode, i, sums, counts):
            if t is None:
                return
            if i < len(sums):
                sums[i] += t.val
                counts[i] += 1
            else:
                sums.append(t.val)
                counts.append(1)
            average(t.left, i+1, sums, counts)
            average(t.right, i+1, sums, counts)
        res = []
        counts = []
        average(root, 0, res, counts)
        for i in range(len(res)):
            res[i] = res[i] / counts[i]
        return res