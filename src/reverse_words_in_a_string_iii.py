# url: https://leetcode.com/problems/reverse-words-in-a-string-iii/
# writtenby: datla

class Solution:
    def reverseWords(self, s: str) -> str:
        return " ".join([x[::-1] for x in s.split(" ")])