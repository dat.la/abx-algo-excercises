# url: https://leetcode.com/problems/invert-binary-tree/
# writtenby: datla

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        if root is None:
            return
        q = [root]
        while q:
            current = q.pop(0)
            current.left, current.right = current.right, current.left            
            if current.left:
                q.append(current.left)
            if current.right:
                q.append(current.right)
        return root