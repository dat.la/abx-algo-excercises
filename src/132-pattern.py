# url: https://leetcode.com/problems/132-pattern/
# writtenby: datla

class Solution:
    def find132pattern(self, nums: List[int]) -> bool:
        s =[]
        mins = -inf
        for i in range(len(nums)-1,-1,-1):
            if nums[i] < mins:
                return True
            while s and nums[i] > s[-1]:
                mins = s.pop()
            s.append(nums[i])
        return False