# url: https://leetcode.com/problems/max-consecutive-ones/
# writtenby: datla
 
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        c ,n = 0, 0
        for x in nums:
            if x == 1:
                c +=1
            if x == 0:
                n = max(c,n)
                c = 0
        return max(c,n)