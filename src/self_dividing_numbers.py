# url: https://leetcode.com/problems/self-dividing-numbers/
# writtenby: datla

class Solution:
    def selfDividingNumbers(self, left: int, right: int) -> List[int]:
        l =[]
        for x in range (left,right+1):
            c = 0
            if "0" in str(x):
                continue
            for i in str(x):
                if x % int(i) == 0:
                    c+=1
            if c == len(str(x)):
                l.append(x)
        return l