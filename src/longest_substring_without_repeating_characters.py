# url: https://leetcode.com/problems/longest-substring-without-repeating-characters/
# writtenby: datla

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        m = 0
        l = []
        for x in s:
            if x in l:
                m = max(len(l),m) + len(l) - 1 - l.index(x)
                l = []
                l.append(x)
            else:
                l.append(x)
        return max(len(l),m)