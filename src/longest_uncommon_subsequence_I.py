# url: https://leetcode.com/problems/longest-uncommon-subsequence-i/
# writtenby: datla

class Solution:
    def findLUSlength(self, a: str, b: str) -> int:
        if a == b:
            return -1
        else:
            return len(a) if len(a) > len(b) else len(b)