# url: https://leetcode.com/problems/number-complement/
# writtenby: datla

class Solution:
    def findComplement(self, num: int) -> int:
        return int("".join(["0" if x == "1" else "1" for x in str(bin(num))[2:]]),2)