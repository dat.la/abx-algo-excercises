# url: https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation/
# writtenby: datla

class Solution:
    def countPrimeSetBits(self, L: int, R: int) -> int:
        c = 0
        l = ["2","3","5","7","11","13","17","19"]
        for x in range(L,R+1):
            s = str(bin(x).count("1"))
            if s in l:
                c += 1
        return c