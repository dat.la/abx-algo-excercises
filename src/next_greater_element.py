# url: https://leetcode.com/problems/next-greater-element-i/
# writtenby: datla

class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        res = []
        for x in nums1:
            n = nums2.index(x)
            if n == len(nums2)-1:
                res.append(-1)
            else:
                while n != len(nums2)-1:
                    if nums2[n+1] > x:
                        res.append(nums2[n+1])
                        break
                    n += 1
                else:
                    res.append(-1)
        return res