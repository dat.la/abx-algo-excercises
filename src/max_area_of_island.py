# url: https://leetcode.com/problems/max-area-of-island/
# writtenby: datla

class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        r = len(grid)
        c = len(grid[0])
        m = 0
        def edge(grid,i,j):
            if i >= r or i < 0 or j >= c or j < 0 or grid[i][j] == 0:
                return 0
            else: grid[i][j] = 0
            return edge(grid,i-1,j)+edge(grid,i+1,j)+edge(grid,i,j-1)+edge(grid,i,j+1)+1
        for i in range(r):
            for j in range(c):
                if grid[i][j] == 1:
                    m = max(edge(grid,i,j),m)
        return m