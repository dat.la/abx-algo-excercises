# url: https://leetcode.com/problems/binary-tree-inorder-traversal/
# writtenby: datla

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        s = []
        p = []
        while True:
            if root is not None:    
                s.append(root)
                root = root.left
            elif(s):
                root = s.pop()
                p.append(root.val)
                root = root.right
            else:
                break
        return p