# url: https://leetcode.com/problems/bag-of-tokens/
# writtenby: datla

class Solution:
    def bagOfTokensScore(self, tokens: List[int], P: int) -> int:
        tokens.sort()
        score = 0
        while len(tokens) > 0:
            if P >= tokens[0]:
                P -= tokens[0]
                score += 1
                tokens.pop(0)
            elif len(tokens) > 2 and score > 0:
                score -= 1
                P += tokens[-1]
                tokens.pop()
            else:
                break
        return score
        