# url: https://leetcode.com/problems/keyboard-row/
# writtenby: datla

class Solution:
    def findWords(self, words: List[str]) -> List[str]:
        l = ["qwertyuiop","asdfghjkl","zxcvbnm"]
        r = []
        num = 0
        for x in words:
            c = 0
            if x[0].lower() in l[0]:
                num = 0
            elif x[0].lower() in l[1]:
                num = 1
            else:
                num = 2
            for ch in x:
                if ch.lower() not in l[num]:
                    c = 1
                    break
            if c == 0:
                r.append(x)
        return r