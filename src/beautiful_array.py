# url: https://leetcode.com/problems/beautiful-array/
# writtrenby: datla

class Solution:
    def beautifulArray(self, N: int) -> List[int]:
        A = [1]
        while len(A) < N:
            evens = [2*x for x in A]
            odds  = [2*x-1 for x in A]
            A = evens + odds
        return [i for i in A if i <= N]