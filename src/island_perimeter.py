# url: https://leetcode.com/problems/island-perimeter/
# writtenby: datla

class Solution:
    def islandPerimeter(self, grid: List[List[int]]) -> int:
        p = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 1:
                    p +=4
                    if  i < len(grid)-1:
                        if grid[i+1][j] == 1:
                            p -=2    
                    if j < len(grid[0])-1:
                        if grid[i][j+1] == 1: 
                            p -=2
        return p