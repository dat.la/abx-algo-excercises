# url: https://leetcode.com/problems/baseball-game/
# writtenby: datla

class Solution:
    def calPoints(self, ops: List[str]) -> int:
        s = 0
        l = []
        for i in range(len(ops)):
            if ops[i] == "+":
                s += l[-1] + l[-2]
                l.append(l[-1]+l[-2])
            if ops[i] == "C":
                s -= l[-1]
                l.pop(-1)
            if ops[i] == "D":
                s += 2*l[-1]
                l.append(2*l[-1])
            if ops[i] not in ["+","C","D"]:
                s += int(ops[i])
                l.append(int(ops[i]))
        return s