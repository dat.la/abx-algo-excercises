# url: https://leetcode.com/problems/reshape-the-matrix/
# writtenby: datla

class Solution:
    def matrixReshape(self, nums: List[List[int]], r: int, c: int) -> List[List[int]]:
        f = []
        b = [[0 for i in range(c)] for j in range(r)]
        x = len(nums)*len(nums[0])
        n = 0
        if x != r*c:
            return nums
        else:
            for row in nums:
                f += row
            for i in range(r):
                for j in range (c):
                    b[i][j] = f[n]
                    n += 1
        return b