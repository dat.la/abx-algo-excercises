# url: https://leetcode.com/problems/maximum-depth-of-binary-tree/
# writtenby: datla

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        def depth(root):
            if root is None:
                return 0
            else:
                left = depth(root.left)
                right = depth(root.right)
                if left > right :
                    return left +1
                else:
                    return right +1
        return depth(root)